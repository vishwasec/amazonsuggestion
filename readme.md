# Amazon search stats

### Get "/search/suggestions"
This would get top ten results from amazon api. http://completion.amazon.com/search/complete?search-alias=aps&client=amazon-search-ui&mkt=1&q=%s


### Post "/save/search"
This would save the search results from the above api.

### Get "/estimate"
This would give the estimate of number of times the word was searched.

### How to run

'mvn spring-boot:run'

Then once the application is up and running, you can use 
sellics.postman_collection.json for postman or
http://localhost:8080/swagger-ui.html#/

Swagger is better, you can quickly try the endpoints by 'try it now' option.

### Assumptions made
* FE uses the /search/suggestions to get top ten words from the suffix.
* Saves the data which it got from the above step.

### Improvements
* Should have used trie to save word with count.


