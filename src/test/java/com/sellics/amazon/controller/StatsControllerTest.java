package com.sellics.amazon.controller;

import com.sellics.amazon.AmazonApplicationTests;
import com.sellics.amazon.service.StatsService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(AmazonApplicationTests.class)
public class StatsControllerTest {
    private MockMvc mvc;

    @Mock
    private StatsService statsService;

    @InjectMocks
    private StatsController statsController;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(statsController)
                .build();
    }

    @Test
    public void getStats() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/estimate?keyword=redbull")
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
}