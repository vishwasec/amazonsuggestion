package com.sellics.amazon.controller;

import com.sellics.amazon.AmazonApplicationTests;
import com.sellics.amazon.service.AmazonSearchService;
import com.sellics.amazon.service.StatsService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(AmazonApplicationTests.class)
public class SearchControllerTest {
    private MockMvc mvc;

    @Mock
    private AmazonSearchService amazonSearchService;
    @Mock
    private StatsService statsService;

    @InjectMocks
    private SearchController searchController;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(searchController)
                .build();
    }

    @Test
    public void search() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/save/search?searchString=redbull")
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchSuggestions() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/search/suggestions?searchString=redbull")
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
}