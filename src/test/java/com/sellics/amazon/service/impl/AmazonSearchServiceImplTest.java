package com.sellics.amazon.service.impl;

import com.sellics.amazon.service.AmazonSearchService;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

public class AmazonSearchServiceImplTest {
    private AmazonSearchService amazonSearchService = new AmazonSearchServiceImpl();

    @Test
    public void getSuggestionsForString() {
        ReflectionTestUtils.setField(amazonSearchService, "amazonUrl",
                "http://completion.amazon.com/search/complete?search-alias=aps&client=amazon-search-ui&mkt=1&q=%s");
        List<String> suggestions = amazonSearchService.getSuggestionsForString("red");
        Assert.assertNotNull(suggestions);
        Assert.assertEquals(10, suggestions.size());
    }
}