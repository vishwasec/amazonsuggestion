package com.sellics.amazon.service.impl;

import com.sellics.amazon.controller.dto.StatsResponseDto;
import com.sellics.amazon.service.StatsService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class StatsServiceImplTest {

    private StatsService statsService = new StatsServiceImpl();

    @Before
    public void init() {
        statsService.save("redbull");
        statsService.save("red");
        statsService.save("redbull");
        statsService.save("redbull");
        statsService.save("redbull");
    }

    @Test
    public void getStats() {
        StatsResponseDto statsResponseDto = statsService.getStats("red");
        Assert.assertNotNull(statsResponseDto);
        Assert.assertEquals(statsResponseDto.getScore(), 20);
    }

    @Test
    public void save() {
    }
}