package com.sellics.amazon.service;

import java.util.List;

public interface AmazonSearchService {

    List<String> getSuggestionsForString(String searchString);
}
