package com.sellics.amazon.service.impl;

import com.sellics.amazon.service.AmazonSearchService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AmazonSearchServiceImpl implements AmazonSearchService {

    @Value("${amazon.url}")
    private String amazonUrl;

    @Override
    public List<String> getSuggestionsForString(String searchString) {
        log.info("Getting suggestions from amazon.");
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response
                = restTemplate.getForEntity(String.format(amazonUrl, searchString)
                , String.class);

        if (null != response.getBody()) {
            String[] responsearray = response.getBody().split("\\[");
            if (responsearray.length > 3) {
                return Arrays.asList(responsearray[2]
                        .replace("],", "")
                        .replace("\"", "")
                        .split(","));
            }
        }

        return Collections.emptyList();
    }
}
