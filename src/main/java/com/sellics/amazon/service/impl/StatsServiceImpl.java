package com.sellics.amazon.service.impl;

import com.sellics.amazon.cache.SaveEntity;
import com.sellics.amazon.controller.dto.StatsResponseDto;
import com.sellics.amazon.service.StatsService;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class StatsServiceImpl implements StatsService {
    private SaveEntity<String, Integer> saveEntity = new SaveEntity<>();

    @Override
    public StatsResponseDto getStats(String searchString) {

        if (saveEntity.getTotal() == 0) return StatsResponseDto.builder().build();

        int percentage = (zeroIfNull(saveEntity.get(searchString)) * 100 / saveEntity.getTotal());
        return StatsResponseDto.builder().keyword(searchString).score(percentage).build();
    }

    @Override
    public void save(String searchString) {
        log.info("Saving string {}", searchString);
        saveEntity.put(searchString, zeroIfNull(saveEntity.get(searchString)) + 1);
    }

    private int zeroIfNull(Integer bonus) {
        return (bonus == null) ? 0 : bonus;
    }
}
