package com.sellics.amazon.service;

import com.sellics.amazon.controller.dto.StatsResponseDto;

public interface StatsService {
    StatsResponseDto getStats(String searchString);

    void save(String searchString);
}
