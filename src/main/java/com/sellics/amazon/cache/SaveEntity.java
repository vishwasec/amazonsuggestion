package com.sellics.amazon.cache;

import java.util.HashMap;
import java.util.Map;

public class SaveEntity<T, Y> {
    private final Map<T, Y> cache = new HashMap<>();
    private int total = 0;

    public void put(T t, Y y) {
        cache.put(t, y);
        total++;
    }

    public Y get(T t) {
        return cache.get(t);
    }

    public int getTotal() {
        return total;
    }

}
