package com.sellics.amazon.controller;

import com.sellics.amazon.service.AmazonSearchService;
import com.sellics.amazon.service.StatsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class SearchController {
    private final AmazonSearchService amazonSearchService;
    private final StatsService statsService;

    @Autowired
    public SearchController(AmazonSearchService amazonSearchService, StatsService statsService) {
        this.amazonSearchService = amazonSearchService;
        this.statsService = statsService;
    }

    /**
     * Saves the searched string.
     */
    @PostMapping("/save/search")
    public void search(@RequestParam(name = "searchString") String searchString) {
        statsService.save(searchString);
    }

    /**
     * Gets list of suggestions for given strings.
     */
    @GetMapping("/search/suggestions")
    public List<String> searchSuggestions(@RequestParam(name = "searchString") String searchString) {
        return amazonSearchService.getSuggestionsForString(searchString);
    }
}
