package com.sellics.amazon.controller.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StatsResponseDto {
    private String keyword;
    private int score;
}
