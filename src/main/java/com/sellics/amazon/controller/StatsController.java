package com.sellics.amazon.controller;

import com.sellics.amazon.controller.dto.StatsResponseDto;
import com.sellics.amazon.service.StatsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatsController {
    private final StatsService statsService;

    @Autowired
    public StatsController(StatsService statsService) {
        this.statsService = statsService;
    }

    @GetMapping("/estimate")
    public StatsResponseDto getStats(@RequestParam(name = "keyword") String keyword) {
        return statsService.getStats(keyword);
    }
}
